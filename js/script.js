function openTab(event, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    /*tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }*/

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(tabName).style.display = "block";
    //event.currentTarget.className += " active";
}

function initTabs() {
    var tabcontent;

    tabcontent = document.getElementsByClassName("tabcontent");
    for(i = 0; i < tabcontent.length; i++) {
        if(tabcontent[i].id != "news")
            tabcontent[i].style.display = "none";
    }
}

document.addEventListener("DOMContentLoaded", initTabs);

var activeMenuElement;
function onMenuHover(object, action) {
    if(action === "in" && activeMenuElement === object)
        return;

    let image;
    for(let i = 0; i < object.childNodes.length; i++)
    {
        if(object.childNodes[i].tagName === "IMG")
        {
            image = object.childNodes[i];
            break;
        }
    }
    if(image) {
        switch (action) {
            case "in":
                image.src = image.src.substring(0, image.src.length - 4) + "-active.png";
                activeMenuElement = object;
                break;
            case "out":
                image.src = image.src.substring(0, image.src.length - 11) + ".png";
                activeMenuElement = null;
                break;
        }
    }
}

document.addEventListener("DOMContentLoaded", getRandomHeader);

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * ((max + 1) - min) + min);
}

function getRandomHeader() {
    let random = getRandomNumber(1, 3);
    console.log("Header: ", random);
    let header = document.getElementById("header-image");
    header.src = header.src.substring(0, header.src.length - 4) + random + ".png";

}